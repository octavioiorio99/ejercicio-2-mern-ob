import { GoodbyeResponse } from "./types";
import { IGoodbyeController } from "./interfaces";
import { LogSuccess } from "../utils/logger";

export class GoodbyeController implements IGoodbyeController {

    public async getMessage(name?: string): Promise<GoodbyeResponse> {
        let date = new Date();
        LogSuccess('[/api/goodbye] Get Request');

        return {
            message: `Goodbye, ${name || "Anonymous"}!`,
            data: `${date.getDate()}-${date.getMonth() + 1}-${date.getFullYear()}`
        }
    }
    
}