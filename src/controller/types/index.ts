/**
 * Basic JSON response for Controllers
 */
export type BasicResponse = {
    message: string
}

/**
 * Error JSON response for Controllers
 */
export type ErrorResponse = {
    error: string,
    message: string
}

/**
 * Basic Goodbye message with Date response for controllers
 */
export type GoodbyeResponse = {
    message: string,
    data: string
}